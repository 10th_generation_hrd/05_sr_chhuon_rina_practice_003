import React, { Component } from "react";
import { Container, Button, Table, Card, Row, Col } from "react-bootstrap";
import Coca from '../image/coca.jpeg'

export default class Components extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          id: 1,
          name: "Coca Cola",
          isSelect: false,
        },
        {
          id: 2,
          name: "Pepsi",
          isSelect: false,
        },
        {
          id: 3,
          name: "Sting",
          isSelect: false,
        },
        {
          id: 4,
          name: "Milk",
          isSelect: true,
        },
      ],
    };
  }

  SelectButton(a) {
    this.setState((falseSelect) => {
      return { result: [(falseSelect.data[a].isSelect = false)] };
    });
  }
  UnSelectButton(a) {
    this.setState((falseSelect) => {
      return { result: [(falseSelect.data[a].isSelect = true)] };
    });
  }

  render() {
    return (
      <div className="App">
        <Container style={{ border: "2px solid black" }}>
          <h1 style={{ textAlign: "center" }}>Welcome to MY Shop</h1>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Produce Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {this.state.data.map((item, b) => {
                return (
                  <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                      {item.isSelect ? (
                        <Button
                          variant="success"
                          onClick={() => this.SelectButton(b)}
                        >
                          Select
                        </Button>
                      ) : (
                        <Button
                          variant="danger"
                          onClick={() => this.UnSelectButton(b)}
                        >
                          Unselect
                        </Button>
                      )}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>

          <Row>
            {this.state.data.map((item) => (
              <Col>
                <Card style={{ width: "19rem" }}>
                  <Card.Img
                    variant="top"
                    src={Coca}
                  />
                  <Card.Body>
                    <Card.Title>
                      <h5>{item.id}</h5>
                      <h5>{item.name}</h5>
                      <h5>
                        {item.isSelect ? (
                          <Button disabled variant="success" onClick={this.onClick}>
                            Select
                          </Button>
                        ) : (
                          <Button disabled variant="danger" onClick={this.onClick}>
                            Unselect
                          </Button>
                        )}
                      </h5>
                    </Card.Title>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
        </Container>
      </div>
    );
  }
}
